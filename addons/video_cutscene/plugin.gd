@tool
extends EditorPlugin


func _enter_tree():
	add_custom_type("VideoCutscene", "VideoStreamPlayer",
	preload("res://addons/video_cutscene/nodes/video_cutscene.gd"),
	preload("res://addons/video_cutscene/video_cutscene.svg"))


func _exit_tree():
	remove_custom_type("VideoCutscene")