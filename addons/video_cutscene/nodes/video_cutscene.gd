extends VideoStreamPlayer

@export var next_scene: PackedScene


func _input(event: InputEvent):
	# Skip the cutscene
	if event.is_action_pressed("ui_accept") and is_playing():
		_change_scene()


func _on_video_stream_player_finished():
	_change_scene()


func _change_scene():
	# Remove the cutscene node
	if next_scene:
		get_tree().call_deferred("change_scene_to_packed", next_scene)
	call_deferred("free")

